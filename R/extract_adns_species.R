#' Extract species from ADNS data
#'
#' @param data A data frame with ADNS data.
#'
#' @return A character vector
#'
#' @export

extract_adns_species <- function(data) {

  startStr <- "Cattle(?!.)"
  endStr <- "Other(?!.)"

  indStartCol <- grep(pattern = startStr, x = colnames(data), ignore.case = TRUE, perl = TRUE)
  if (is.na(indStartCol[1])) stop("No species found in ADNS data")
  indStartCol <- indStartCol[1]

  indEndCol <- grep(pattern = endStr, x = colnames(data), ignore.case = TRUE, perl = TRUE)
  if (is.na(indEndCol[1])) stop("No species found in ADNS data")
  indEndCol <- indEndCol[length(indEndCol)]

  allSpeciesDF <- data[, indStartCol:indEndCol]

  # Keep only columns starting with X and number
  indXSpec <- grep(pattern = "^X\\d+\\.", x = colnames(allSpeciesDF), ignore.case = TRUE, perl = TRUE)
  if (is.na(indXSpec[1])) stop("No species found in ADNS data")

  allSpeciesDF <- allSpeciesDF[, indXSpec]


  nbOutb <- nrow(data)

  species <- rep(NA, nbOutb)


  for(k in 1:nbOutb) {

    indSpec <- which(!is.na(t(allSpeciesDF[k, ])))

    tempSpec <- colnames(allSpeciesDF[k, indSpec, drop = FALSE])

    tempSpec <- gsub("X\\d+\\.", "", tempSpec)

    tempSpec <- gsub("\\.", " ", tempSpec)

    finalSpec <- unique(tempSpec)

    if (length(finalSpec) == 1) species[k] <- finalSpec else species[k] <- paste(finalSpec, collapse = "-")


  }

  return(species)

}

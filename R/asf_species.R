#' Define species for african swine fever: wild boar or domestic pigs
#'
#' @param data A data frame with at least 2 columns with names Disease and Species.
#'
#' @return A data frame with 2 new columns SpeciesENG and SpeciesFR
#'
#' @export



asf_species <- function(data) {

  if (any(colnames(data) %in% c("SpeciesENG", "SpeciesFR"))) {

    print("SpeciesENG and SpeciesFR already defined for this data frame")
    return(data)

  }

  # Check presence of species column
  data <- check_species(data)


  # Species names
  strWildENG <- "Wild boar"
  strWildFR <- "Sauvages"
  strDomENG <- "Domestic pigs"
  strDomFR <- "Elevages"


  # Remove NA Species
  indNA <- which(is.na(data$Species))
  if (!is.na(indNA[1])) data <- data[-indNA, ]

  data$SpeciesENG <- data$Species
  data$SpeciesFR <- data$Species

  # Rename Species for Empres-i source
  indSauvSc <- grep("wild", data$Species, ignore.case = TRUE, perl = TRUE)
  data[indSauvSc, "SpeciesENG"] <-  strWildENG
  data[indSauvSc, "SpeciesFR"] <-  strWildFR

  indElevSc <- grep("domestic", data$Species, ignore.case = TRUE, perl = TRUE)
  data[indElevSc, "SpeciesENG"] <-  strDomENG
  data[indElevSc, "SpeciesFR"] <-  strDomFR

  # Rename Species for ADNS source
  indSauv <- grep("wild", data$Disease, ignore.case = TRUE, perl = TRUE)
  data[indSauv, "SpeciesENG"] <-  strWildENG
  data[indSauv, "SpeciesFR"] <-  strWildFR

  indElev <- grep("domestic", data$Disease, ignore.case = TRUE, perl = TRUE)
  data[indElev, "SpeciesENG"] <-  strDomENG
  data[indElev, "SpeciesFR"] <-  strDomFR

  data <- subset(data, select = -Species)

  return(data)

}

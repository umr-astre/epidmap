
Epidmap
-------

Epidmap is a collection of tools to produce outbreaks maps. This R package is used by the French Animal Health Epidemic Intelligence team (in french: *Veille Sanitaire Internationale*) of the [ESA platform](https://www.plateforme-esa.fr/).

### Installation

``` r
if (!require("remotes")) {
  install.packages("remotes")
}

remotes::install_gitlab("umr-astre/epidmap", host = "forgemia.inra.fr")
```

Package rnaturalearthdata &gt;= 0.2.0 is required. Please install last version from Github :

``` r
remotes::install_github("ropensci/rnaturalearthdata")
```

### Density map

``` r
library(epidmap)

# Define outbreaks file: african swine fever in Europe 2014-aug2019, raw data from FAO Empres-i
outbfile <- system.file("extdata", "Empres-i_ASF_Europe_2014-2019.csv", package = "epidmap")

# Open file and extract data of interest
asfDF <- open_outbreakfile(outbfile)

# Remove longitude > 55
indrem <- which(asfDF$Longitude > 55)
if (!is.na(indrem[1])) asfDF <- asfDF[-indrem, ]

# Compute density map 
map_density(data = asfDF,
            map.lang = "en",
            map.extent.km = c(-1900, -800, 100, 100),
            hex.size.km = 100, 
            country.bound.color = "white",
            country.bound.alpha = 0.2,
            country.fill.color = rgb(0, 114, 119, max = 255),
            water.fill.color = "#D6EEFF",
            country.names.color = "white",
            country.names.alpha = 0.9,
            scalebar.color = "white",
            north.shift.km = c(-200, 0))
```

<img src="README_files/figure-markdown_github/density-1.png" style="display: block; margin: auto;" />

### Points map

``` r
library(epidmap)

# Define outbreaks file: HPAI H5N6 and H5N8 in Europe during 2018, raw data from FAO Empres-i
outbfile <- system.file("extdata", "Empres-i_H5N6-8_Europe_2018.csv", package = "epidmap")

# Open file and extract data of interest
hpaiDF <- open_outbreakfile(outbfile)

# Compute points map 
map_points(data = hpaiDF,
           map.lang = "fr",
           map.extent.km = c(-300, -850, 200, 400),
           point.size = 3,
           point.fill.colors = c("#1a9850", "#d73027"),
           point.fill.variable = "Serotype",
           leg.fill.title = "Sérotype",
           north.shift.km = c(-150, 0))
```

<img src="README_files/figure-markdown_github/points-1.png" style="display: block; margin: auto;" /> Source: [FAO Empres-i](http://empres-i.fao.org/eipws3g/)
